# MRI Brain Tumor detection
The aim of this project is to develop a Deep Learning Convolutional Neural Network to classify MRI images whether they show brain tumor or not. The original data set for this work has been collected from Kaggle.com

# Original data set
https://www.kaggle.com/navoneel/brain-mri-images-for-brain-tumor-detection

# Project overview
1. Necessary libraries are imported
2. The data set is read and then divided into sub-directories containing the train, test and validation data.

Train data: Used to train the CNN model
Validation data: Used to check the model performance while training
Test data: An independent data set used to check the model performance

3. Data pre-processing
A. Finding the brain image contours and extreme points of the contour, to crop out only the brain part from the entire image
B. Data normalization to have a mean of zero and std. deviation of 1 for each individual color channel in each image.

4. Training the model using a CNN architecture and validating the model
