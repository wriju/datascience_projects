# Project Overview

The aim of this project is to identify metastatic cancer from images of tissues taken from the larger digital pathology scans. 

In this project a simple convolutional neural network is built and trained to identify whether the image of the tissue shows cancer or not, i.e. the trained model is aimed towards a binary classification problem. 

# Description of the method

The images are loaded and an exloratory data analysis is performed to examine the different color channels. In the training data set, there is a class imbalance amongst the two categories of images: cancerous image and non-cancerous image. Hence while building the CNN model, a weighted class label is added giving higher weight to the more under-represented class.

The CNN model architecture was found by experimenting with different number of convolutional layers, different number of units in each layer and different methods of regularization to prevent overfitting (for e.g. Early Stopping). Accuracy was used as the metric to evaluate the validation data set while training. The highest accuracy reached for the validation data set was ~0.83.

A separate evaulation is done on a test data set which is not included while training. ROC AUC Score is used to evaluate the test set. The score obtained was ~0.9.

# Data set
The data set used for this project was obtained from the following Kaggle challenge: https://www.kaggle.com/c/histopathologic-cancer-detection/overview 
